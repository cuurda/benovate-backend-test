# coding: utf-8

"""Serializers for queries."""

from rest_framework import serializers
from rest_framework.serializers import ValidationError

from benovate_backend_test.models import (
    ACCOUNT_DECIMAL_PLACES,
    ACCOUNT_WHOLE_PLACES,
    VALID_ITN_LENGTHS,
    AccountUser,
)


class TransactionSerializer(serializers.Serializer):
    """Serializer for money transaction."""

    def __init__(self, *args, **kwargs):
        """
        Initialize transaction serializer.

        Dynamic username choices render.
        """
        super().__init__(*args, **kwargs)
        self.fields['username'] = serializers.ChoiceField(
            required=True,
            choices=AccountUser.objects.get_choices(),
        )
        self.fields['itn'] = serializers.CharField(required=True)

        self.fields['amount'] = serializers.DecimalField(
            max_digits=ACCOUNT_WHOLE_PLACES + ACCOUNT_DECIMAL_PLACES,
            decimal_places=ACCOUNT_DECIMAL_PLACES,
        )

    def validate_itn(self, itn):
        """Validate ITN field."""
        if not itn.isnumeric():
            raise ValidationError(
                'ITN must contain only digits.',
            )
        if len(itn) not in VALID_ITN_LENGTHS:
            raise ValidationError(
                'ITN must have 10 digits.',
            )
        itn_users = AccountUser.objects.filter(itn=itn)
        if not len(itn_users):
            raise ValidationError(
                'ITN is not in database.',
            )
        return itn

    def validate(self, transaction):
        """Validate transaction."""
        user = self.check_user_exist(transaction)
        return self.check_user_ammount(transaction, user)

    def check_user_exist(self, transaction):
        """Check if user exist."""
        try:
            return AccountUser.objects.get(
                username=transaction['username'],
            )
        except AccountUser.DoesNotExist:
            raise ValidationError("User doesn't exist.")

    def check_user_ammount(self, transaction, user):
        """Check if user has enough money to transfer."""
        if user.has_enough_money(transaction['amount']):
            return transaction
        raise ValidationError("User hasn't enough money")

    def save(self):
        """
        Save transaction.

        Move money.
        """
        transaction = self.validated_data
        print(transaction)
        itn_users = AccountUser.objects.filter(
            itn=transaction['itn'],
        )
        per_user_amount = transaction['amount'] / len(itn_users)
        sender = AccountUser.objects.get(
            username=transaction['username'],
        )
        sender.account -= transaction['amount']
        sender.save()
        for itn_user in itn_users:
            itn_user.account += per_user_amount
            itn_user.save()
