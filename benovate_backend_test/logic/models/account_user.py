# coding: utf-8

"""User with account model."""

from django.contrib.auth.models import User, UserManager
from django.db import models

MAX_ITN_LENGTH = 10
VALID_ITN_LENGTHS = (10,)
ACCOUNT_DECIMAL_PLACES = 2
ACCOUNT_WHOLE_PLACES = 3


class AccountUserManager(UserManager):
    """AccountUser custom manager."""

    def get_choices(self):
        """Get choices for selector."""
        all_users = super().get_queryset().all()
        choices = []
        for user in all_users:
            choices.append(user.username)
        return choices


class AccountUser(User):
    """User with account model."""

    itn = models.CharField(max_length=MAX_ITN_LENGTH)
    account = models.DecimalField(
        max_digits=ACCOUNT_WHOLE_PLACES + ACCOUNT_DECIMAL_PLACES,
        decimal_places=ACCOUNT_DECIMAL_PLACES,
    )

    objects = AccountUserManager()

    def has_enough_money(self, ammount):
        """Return has user enough money."""
        return self.account >= ammount
