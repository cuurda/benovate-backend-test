# coding: utf-8

"""Benovate backend test views."""

from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from benovate_backend_test.serializers import TransactionSerializer


class TransactionFormView(APIView):
    """Transaction form view."""

    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'transaction_form.html'

    def get(self, request):
        """Render transaction form."""
        transaction_serializer = TransactionSerializer()
        return Response({
            'transaction_serializer': transaction_serializer,
            'status': 'Form is not processed.',
        })

    def post(self, request):
        """Process transaction form."""
        transaction_serializer = TransactionSerializer(data=request.data)
        if not transaction_serializer.is_valid():
            return Response({
                'transaction_serializer': transaction_serializer,
                'status': 'Form processed with errors.',
            })
        return Response({
            'transaction_serializer': transaction_serializer,
            'status': 'Form processed success.',
        })
