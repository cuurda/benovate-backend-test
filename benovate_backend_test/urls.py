# coding: utf-8

"""
benovate_backend_test URL Configuration.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
"""

from django.contrib import admin
from django.urls import path

from benovate_backend_test import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.TransactionFormView.as_view(), name='transaction'),
]
