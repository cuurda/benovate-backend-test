#!/bin/bash

set -e
postgres_ready () {
  # Check that postgres is up and running on port `5432`:
  sh '/code/wait-for-command.sh' -t 5 -s 0 52 -c "curl $host"
}

export PATH=$PATH:/usr/local/bin

if [[ ! -z "${WAIT_FOR_DATABASE}" ]]; then
  host="postgresql:5432"
  until postgres_ready; do
    echo "Postgres is unavailable - sleeping"
    sleep 1
  done
fi 

echo "Postgres is up - executing command"

#while true; do sleep 86400; done

python3 manage.py makemigrations benovate_backend_test && \
   python3 manage.py migrate && \
   python3 manage.py collectstatic --noinput

if [ "$@"="" ]; then
    exec gunicorn benovate_backend_test.wsgi -b 0.0.0.0:$PORT --log-file -
else
    exec "$@"
fi