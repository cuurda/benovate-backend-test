# coding: utf-8

"""Tests for serializer."""

import decimal

import pytest
from rest_framework.serializers import ValidationError

from benovate_backend_test.models import AccountUser
from benovate_backend_test.serializers import TransactionSerializer

decimal.getcontext().prec = 2

EXIST_USERNAME = 'big_lebowski'
SECOND_USERNAME = 'diehard'
THIRD_USERNAME = 'sicorsky'
UNEXIST_USERNAME = 'neo'
RIGHT_ITN = '1234567890'
SECOND_USER_ITN = '2234567890'
THIRD_USER_ITN = SECOND_USER_ITN
WRONG_ITN1 = '0987654321'
WRONG_ITN2 = '09876543a1'
WRONG_ITN3 = '098765432'

ACCOUNT_AMOUNT = decimal.Decimal('100.19')
SECOND_ACCOUNT_AMOUNT = decimal.Decimal('50.4')
THIRD_ACCOUNT_AMOUNT = decimal.Decimal('33.3')
TRANSACTION_AMOUNT = decimal.Decimal('50.2')
ALLOW_AMOUNT = decimal.Decimal('10.3')
DENY_AMOUNT = decimal.Decimal('101.0')

TEST_TRANSACTION_INTERNAL = {  # noqa: WPS407
    'username': EXIST_USERNAME,
    'itn': RIGHT_ITN,
    'amount': ALLOW_AMOUNT,
}


@pytest.fixture
def test_transaction():
    """Standart test transaction."""
    return {
        'username': EXIST_USERNAME,
        'itn': RIGHT_ITN,
        'amount': ALLOW_AMOUNT,
    }


@pytest.fixture
def account_user():
    """Standart account."""
    return AccountUser(
        username=EXIST_USERNAME,
        itn=RIGHT_ITN,
        account=ACCOUNT_AMOUNT,
    )


@pytest.fixture
def test_transaction_users(account_user):
    """Test transaction users."""
    return [
        account_user,
        AccountUser(
            username=SECOND_USERNAME,
            itn=SECOND_USER_ITN,
            account=SECOND_ACCOUNT_AMOUNT,
        ),
        AccountUser(
            username=THIRD_ACCOUNT_AMOUNT,
            itn=THIRD_USER_ITN,
            account=THIRD_ACCOUNT_AMOUNT,
        ),
    ]


@pytest.mark.django_db
class TestBenovateSerializers:
    """Test for serializers."""

    def test_transaction_serializer(self, account_user, test_transaction):
        """Test transaction serializer."""
        account_user.save()
        serializer = TransactionSerializer(data=test_transaction)
        assert serializer.initial_data == test_transaction

    def test_transaction_serializer_valid(self, account_user, test_transaction):
        """Test valid transaction serializer."""
        account_user.save()
        serializer = TransactionSerializer(data=test_transaction)
        assert serializer.is_valid(raise_exception=True) is True

    def test_transaction_serializer_invalid_itn(
        self,
        account_user,
        test_transaction,
    ):
        """Test invalid itn transaction serializer."""
        account_user.save()
        test_transaction['itn'] = WRONG_ITN1
        serializer = TransactionSerializer(data=test_transaction)
        with pytest.raises(ValidationError):
            serializer.is_valid(raise_exception=True)
        test_transaction['itn'] = WRONG_ITN2
        serializer = TransactionSerializer(data=test_transaction)
        with pytest.raises(ValidationError):
            serializer.is_valid(raise_exception=True)
        test_transaction['itn'] = WRONG_ITN3
        serializer = TransactionSerializer(data=test_transaction)
        with pytest.raises(ValidationError):
            serializer.is_valid(raise_exception=True)

    def test_transaction_serializer_invalid_user(
        self,
        test_transaction,
    ):
        """Test invalid user transaction serializer."""
        test_transaction['username'] = UNEXIST_USERNAME
        serializer = TransactionSerializer(data=test_transaction)
        with pytest.raises(ValidationError):
            serializer.is_valid(raise_exception=True)

    def test_transaction_serializer_invalid_sum(
        self,
        account_user,
        test_transaction,
    ):
        """Test valid itn transaction serializer."""
        account_user.save()
        test_transaction['amount'] = DENY_AMOUNT
        serializer = TransactionSerializer(data=test_transaction)
        with pytest.raises(ValidationError):
            serializer.is_valid(raise_exception=True)

    def test_transaction_serializer_valid_user(
        self,
        account_user,
        test_transaction,
    ):
        """Test valid user transaction serializer."""
        account_user.save()
        serializer = TransactionSerializer(data=test_transaction)
        user = serializer.check_user_exist(TEST_TRANSACTION_INTERNAL)
        assert user == account_user

    def test_transaction_serializer_enough_money(self, account_user):
        """Test valid user transaction serializer."""
        account_user.save()
        serializer = TransactionSerializer(data=test_transaction)
        user = serializer.check_user_exist(TEST_TRANSACTION_INTERNAL)
        transaction = serializer.check_user_ammount(
            TEST_TRANSACTION_INTERNAL,
            user,
        )
        assert transaction == TEST_TRANSACTION_INTERNAL

    def test_transaction_serializer_save(self, test_transaction_users):
        """Test transaction save."""
        for user in test_transaction_users:
            user.save()
        serializer = TransactionSerializer(data={
            'username': test_transaction_users[0].username,
            'itn': test_transaction_users[1].itn,
            'amount': TRANSACTION_AMOUNT,
        })
        assert serializer.is_valid(raise_exception=True) is True
        serializer.save()
        new_zero_user_account = ACCOUNT_AMOUNT - TRANSACTION_AMOUNT
        new_first_user_account = SECOND_ACCOUNT_AMOUNT + TRANSACTION_AMOUNT / 2
        new_second_user_account = THIRD_ACCOUNT_AMOUNT + TRANSACTION_AMOUNT / 2
        for index, user in enumerate(test_transaction_users):
            test_transaction_users[index] = AccountUser.objects.get(
                pk=user.pk,
            )
        assert test_transaction_users[0].account == new_zero_user_account
        assert test_transaction_users[1].account == new_first_user_account
        assert test_transaction_users[2].account == new_second_user_account
