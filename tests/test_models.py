# coding: utf-8

"""Tests for model."""

import pytest
from mixer.backend.django import mixer

from benovate_backend_test.models import AccountUser


@pytest.fixture
def account_user():
    """Standart account."""
    return AccountUser(itn='1234567890', account=100.19)


@pytest.mark.django_db
class TestBenovateModels:
    """Test benovate backend test models."""

    def test_account_user(self):
        """Test user creation."""
        old_count = AccountUser.objects.count()
        mixer.blend('benovate_backend_test.AccountUser')
        assert AccountUser.objects.count() == old_count + 1

    def test_user_get_choices(self, account_user):
        """Test user choices."""
        account_user.save()
        choices = AccountUser.objects.get_choices()
        assert len(choices) == 1
        assert choices[0] == account_user.username


class TestBenovateModelMethods:
    """Test benovate backend test model methods."""

    def test_account_user_has_enough_money(self, account_user):
        """Test user has enough money."""
        assert account_user.has_enough_money(50.09) is True

    def test_account_user_has_enough_all_money(self, account_user):
        """Test user has enough all money."""
        assert account_user.has_enough_money(100.19) is True

    def test_account_user_has_no_enough_money(self, account_user):
        """Test user has no enough money."""
        assert account_user.has_enough_money(101.19) is False
