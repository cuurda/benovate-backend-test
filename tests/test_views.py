# coding: utf-8

"""Tests for view."""

import pytest
from django.test import Client
from django.urls import reverse

from benovate_backend_test.models import AccountUser

EXIST_USERNAME = 'big_lebowski'
UNEXIST_USERNAME = 'neo'
RIGHT_ITN = '1234567890'
ACCOUNT_AMOUNT = 100.19
ALLOW_AMOUNT = 10.3


@pytest.fixture
def test_transaction():
    """Standart test transaction."""
    return {
        'username': EXIST_USERNAME,
        'itn': RIGHT_ITN,
        'amount': ALLOW_AMOUNT,
    }


@pytest.fixture
def account_user():
    """Standart account."""
    return AccountUser(
        username=EXIST_USERNAME,
        itn=RIGHT_ITN,
        account=ACCOUNT_AMOUNT,
    )


@pytest.mark.django_db
class TestBenovateViews:
    """Test benovate backend test views."""

    def test_transaction_form_view_get(self):
        """Test transaction form view get."""
        client = Client()
        response = client.get(
            reverse('transaction'),
        )
        assert response.status_code == 200

    def test_transaction_form_view_post_valid(
        self,
        account_user,
        test_transaction,
    ):
        """Test transaction form view post valid."""
        account_user.save()
        client = Client()
        response = client.post(
            reverse('transaction'),
            data=test_transaction,
        )
        assert response.status_code == 200

    def test_transaction_form_view_post_invalid(
        self,
        account_user,
        test_transaction,
    ):
        """Test transaction form view post invalid."""
        account_user.save()
        test_transaction['username'] = UNEXIST_USERNAME
        client = Client()
        response = client.post(
            reverse('transaction'),
            data=test_transaction,
        )
        assert response.status_code == 200
